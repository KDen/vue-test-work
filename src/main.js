import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import 'buefy/'
import VueTheMask from 'vue-the-mask'

Vue.config.productionTip = false;
Vue.use(Buefy);
Vue.use(VueTheMask);

new Vue({
  router,
  store,
  Buefy,
  render: h => h(App)
}).$mount('#app');