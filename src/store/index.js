import Vue from 'vue'
import Vuex from 'vuex'
import mainForm from '@/store/modules/mainForm'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    mainForm
  }
})
