export default {
  state: {
    persons: [],
    table: {
      isLoading: true
    }
  },
  actions: {
    async fetchPersons(ctx) {
      const res = await fetch('http://192.168.0.2:8080/users.json');
      const persons = await res.json();

      setTimeout(()=> {
        ctx.commit('updatePersonsTable', persons.users);
      }, 1000);
    }
  },
  mutations: {
    updatePersonsTable(state, persons) {
      state.persons = persons;
      state.table.isLoading = false;
    },
    deletePerson(state, id) {
      state.persons = state.persons.filter( i => id !== i.id );
    },
    addPersonToTable(state, person) {
      state.persons.push(person);
    }
  },
  getters: {
    allPersons(state) {
      return state.persons;
    },
    countPersons(state) {
      return state.persons.length
    },
    isTableLoading(state) {
      return state.table.isLoading;
    }
  }
}